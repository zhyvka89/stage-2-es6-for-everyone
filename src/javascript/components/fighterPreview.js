import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} fighters___fighter`,
  });

  fighterElement.append(createFighterImage(fighter), createFighterInfo(fighter))

  return fighterElement;
}

function createFighterInfo(fighter) {
  const fighterInfoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___info',
  });

  const name = createElement({tagName: 'span'});
  name.innerHTML = `<b>${fighter.name}</b>`;

  const health = createElement({tagName: 'span'});
  health.innerHTML = `Health: ${fighter.health}`;

  const attack = createElement({tagName: 'span'});
  attack.innerHTML = `Attack: ${fighter.attack}`;

  const defense = createElement({tagName: 'span'});
  defense.innerHTML = `Defense: ${fighter.defense}`;

  fighterInfoElement.append(name, health, attack, defense);
  
  return fighterInfoElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
