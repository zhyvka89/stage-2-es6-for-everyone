import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstFighterState = {
      ...firstFighter,
      isBlocked: false,
    }

    const secondFighterState = {
      ...secondFighter,
      isBlocked: false,
    }

    const firstFighterHealthBar = document.getElementById('left-fighter-indicator');
    const secondFighterHealthBar = document.getElementById('right-fighter-indicator');

    window.addEventListener('keydown', e => {
      if (e.code === controls.PlayerOneAttack){
        if (!secondFighterState.isBlocked && !firstFighterState.isBlocked) {
          secondFighterState.health -= getDamage(firstFighterState, secondFighterState);
          const healthIndicator = secondFighterState.health < 0 ? 0 : (secondFighterState.health / secondFighter.health) * 100;
          secondFighterHealthBar.style.width = `${healthIndicator}%`;
        }
      }

      if (e.code === controls.PlayerTwoAttack) {
        if (!firstFighterState.isBlocked && !secondFighterState.isBlocked) {
          firstFighterState.health -= getDamage(secondFighterState, firstFighterState);
          const healthIndicator = firstFighterState.health < 0 ? 0 : (firstFighterState.health / firstFighter.health) * 100;
          firstFighterHealthBar.style.width = `${healthIndicator}%`;
        }
      }

      if (e.code === controls.PlayerOneBlock) {
        firstFighterState.isBlocked = true;
      }

      if (e.code === controls.PlayerTwoBlock) {
        secondFighterState.isBlocked = true;
      }

      if (firstFighterState.health <= 0) {
        resolve(secondFighter);
      }

      if (secondFighterState.health <= 0) {
        resolve(firstFighter);
      }
    })

    window.addEventListener('keyup', (e) => {
      if (e.code === controls.PlayerOneAttack) {
        firstFighterState.isAttacking = false;
      }

      if (e.code === controls.PlayerTwoAttack) {
        secondFighterState.isAttacking = false;
      }

      if (e.code === controls.PlayerOneBlock) {
        firstFighterState.isBlocked = false;
      }

      if (e.code === controls.PlayerTwoBlock) {
        secondFighterState.isBlocked = false;
      }
    })

  });
}

export function getDamage(attacker, defender) {
  const damageA = getHitPower(attacker) - getBlockPower(defender);
  console.log(`damage: ${damageA}`);
  const damage = damageA > 0 ? damageA : 0;

  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const hitPower = fighter.attack * criticalHitChance;
  console.log(`hitpower: ${hitPower}`)

  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const blockPower = fighter.defense * dodgeChance;
  console.log(`blockpower: ${blockPower}`)

  return blockPower;
}
