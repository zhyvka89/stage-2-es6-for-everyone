import { createFighterImage } from "../fighterPreview";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const winner = {
    title: `${fighter.name} won`,
    bodyElement: createFighterImage(fighter)
  }
  
  showModal(winner);
}
