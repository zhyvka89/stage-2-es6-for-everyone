import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    const endpoint = `details/fighter/${id}.json`;
    
    try {
      const fighter = await callApi(endpoint);
      return fighter;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
